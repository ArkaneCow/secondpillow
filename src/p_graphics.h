#ifndef graphics_h
#define graphics_h
#include <stdlib.h>
#include <math.h>
#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <GLUT/glut.h>
#include <OpenCL/cl.h>
#else
#include <GL/glut.h>
#include <CL/cl.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "world.h"
#include "p_util.h"
#include "camera.h"
#include "octree.h"
#include "vector_3d.h"
int frame_height;
int frame_width;
float field_of_view;
float* frame;
float* angle_array;
int octree_traverse_depth;
int octree_max_size;
vector_3d* default_vector;
//https://github.com/elazar55/OpenCL-Tests/blob/master/OpenCL/Main.cpp
cl_int error;
cl_platform_id* platforms;
cl_uint num_platforms;
cl_device_id* devices;
cl_uint num_devices;
cl_context context;
cl_command_queue command_queue;
cl_program program;
cl_kernel kernel;
cl_mem angle_array_d;
cl_mem render_width_d;
cl_mem render_height_d;
cl_mem test_d;
cl_mem ray_pos_d;
cl_mem ray_angle_d;
cl_mem traverse_depth_d;
cl_mem render_buffer_d;
void set_solid_color(float r, float g, float b);
void set_pixel_color(float r, float g, float b, int x, int y);
void calculate_angles(int screen_width, int screen_height, float field_of_view);
void renderAABB(float* render_frame, int render_width, int render_height, camera* cam, world* scene, int traverse_depth);
void cl_renderAABB(float* render_frame, int render_width, int render_height, camera* cam, world* scene);
#endif
