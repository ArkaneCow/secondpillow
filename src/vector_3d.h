#ifndef vector_3d_h
#define vector_3d_h
#include <stddef.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct {
    float x;
    float y;
    float z;
} vector_3d;
void vector_3d_init(vector_3d* v, float x, float y, float z);
void vector_3d_init_default(vector_3d* v);
vector_3d* vector_3d_create(float x, float y, float z);
vector_3d* vector_3d_create_default();
float vector_3d_magnitude(vector_3d* a);
vector_3d vector_3d_normalize(vector_3d* a);
vector_3d vector_3d_negative(vector_3d* a);
vector_3d vector_3d_cross_product(vector_3d* a, vector_3d* v);
vector_3d vector_3d_vector_add(vector_3d* a, vector_3d* v);
vector_3d vector_3d_vector_subtract(vector_3d* a, vector_3d* v);
vector_3d vector_3d_vector_multiply(vector_3d* a, float scalar);
vector_3d vector_3d_vector_divide(vector_3d* a, float scalar);
vector_3d vector_3d_rotate(vector_3d* a, float degree_x, float degree_y, float degree_z);
float vector_3d_angle_between(vector_3d* a, vector_3d* b);
vector_3d* rotate_degree_x(vector_3d* o, float degree);
vector_3d* rotate_degree_y(vector_3d* o, float degree);
vector_3d* rotate_degree_z(vector_3d* o, float degree);
#endif