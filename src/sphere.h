#ifndef sphere_h
#define sphere_h

#include "vector_3d.h"

typedef struct {
    vector_3d* origin;
    float radius;
} sphere;

//http://sourceforge.net/p/rasterrain/code/ci/master/tree/Sphere.h with a few tweaks
void sphere_init(sphere* s, vector_3d* origin, float radius);
void sphere_init_default(sphere* s);
sphere* sphere_create(vector_3d* origin, float radius);
sphere* sphere_create_default();
vector_3d sphere_get_normal(sphere* s, vector_3d* point);
float sphere_intersect(sphere* s, vector_3d* r_origin, vector_3d* r_dir);

#endif