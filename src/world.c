#include "world.h"

void world_init(world* scene, vector_pointer* octrees) {
    scene->octrees = *octrees;
}

void world_init_default(world* scene) {
    scene->octrees = *vector_pointer_create();
    printf("scene size is %d\n", scene->octrees.size);
    printf("scene capacity is %d\n", scene->octrees.capacity);
}

world* world_create(vector_pointer* octrees) {
    world* w = (world*) malloc(sizeof (world));
    world_init(w, octrees);
    return w;
}

world* world_create_default() {
    world* w = (world*) malloc(sizeof (world));
    world_init_default(w);
    return w;
}
