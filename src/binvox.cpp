#include <string>
#include <fstream>
#include <iostream>
#include <stdlib.h>
using namespace std;
static int version;
static int depth, height, width;
static int size;
static unsigned char* voxels = 0;
;
static float tx, ty, tz;
static float scale;

int read_binvox(string filespec) {
    ifstream *input = new ifstream(filespec.c_str(), ios::in | ios::binary);
    string line;
    *input >> line; // #binvox
    if (line.compare("#binvox") != 0) {
        cout << "Error: first line reads [" << line << "] instead of [#binvox]" << endl;
        delete input;
        return 0;
    }
    *input >> version;
    cout << "reading binvox version " << version << endl;
    depth = -1;
    int done = 0;
    while (input->good() && !done) {
        *input >> line;
        if (line.compare("data") == 0) done = 1;
        else if (line.compare("dim") == 0) {
            *input >> depth >> height >> width;
        } else if (line.compare("translate") == 0) {
            *input >> tx >> ty >> tz;
        } else if (line.compare("scale") == 0) {
            *input >> scale;
        } else {
            cout << "  unrecognized keyword [" << line << "], skipping" << endl;
            char c;
            do { // skip until end of line
                c = input->get();
            } while (input->good() && (c != '\n'));

        }
    }
    if (!done) {
        cout << "  error reading header" << endl;
        return 0;
    }
    if (depth == -1) {
        cout << "  missing dimensions in header" << endl;
        return 0;
    }
    size = width * height * depth;
    voxels = new unsigned char[size];
    if (!voxels) {
        cout << "  error allocating memory" << endl;
        return 0;
    }
    unsigned char value;
    unsigned char count;
    int index = 0;
    int end_index = 0;
    int nr_voxels = 0;
    input->unsetf(ios::skipws);
    *input >> value;
    while ((end_index < size) && input->good()) {
        *input >> value >> count;
        if (input->good()) {
            end_index = index + count;
            if (end_index > size) return 0;
            for (int i = index; i < end_index; i++) voxels[i] = value;
            if (value) nr_voxels += count;
            index = end_index;
        }
    }
    input->close();
    cout << "  read " << nr_voxels << " voxels" << endl;
    return 1;
}

unsigned char* read_binvox_ii(string filename, int* dim_x, int* dim_y, int* dim_z, float* translate_x, float* translate_y, float* translate_z, float* scale_factor) {
    cout << "read_binvox_ii" << endl;
    int size, depth, width, height, version;
    float tx, ty, tz, scale;
    ifstream* input = new ifstream(filename.c_str(), ios::in | ios::binary);
    string line;
    *input >> line;
    *input >> version;
    depth = -1;
    int done = 0;
    while (input->good() && !done) {
        *input >> line;
        if (line.compare("data") == 0) {
            done = 1;
        } else if (line.compare("dim") == 0) {
            *input >> depth >> height >> width;
        } else if (line.compare("translate") == 0) {
            *input >> tx >> ty >> tz;
        } else if (line.compare("scale") == 0) {
            *input >> scale;
        } else {
            char c;
            do {
                c = input->get();
            } while (input-> good() && c != '\n');
        }
    }
    size = width * height * depth;
    unsigned char* char_vox = new unsigned char[size];
    unsigned char value;
    unsigned char count;
    int index = 0;
    int end_index = 0;
    int nr_char_vox = 0;
    input->unsetf(ios::skipws);
    *input >> value;
    while ((end_index < size) && input->good()) {
        *input >> value >> count;
        if (input->good()) {
            end_index = index + count;
            for (int i = index; i < end_index; i++) {
                char_vox[i] = value;
            }
            if (value) {
                nr_char_vox += count;
            }
            index = end_index;
        }
    }
    input->close();
    (*dim_x) = width;
    (*dim_y) = height;
    (*dim_z) = depth;
    (*translate_x) = tx;
    (*translate_y) = ty;
    (*translate_z) = tz;
    (*scale_factor) = scale;
    return char_vox;
}

extern "C" int read_binvox_c(char* filename, int* o_version, int* o_depth, int* o_height, int* o_width, int* o_size, unsigned char* o_voxels, float* o_tx, float* o_ty, float* o_tz, float* o_scale) {
    string name(filename);
    int i = read_binvox(name);
    *o_version = version;
    *o_depth = depth;
    *o_height = height;
    *o_width = width;
    *o_size = size;
    o_voxels = voxels;
    *o_tx = tx;
    *o_ty = ty;
    *o_tz = tz;
    *o_scale = scale;
    return i;
}

extern "C" unsigned char* read_binvox_ii_c(char* filename, int* dim_x, int* dim_y, int* dim_z, float* translate_x, float* translate_y, float* translate_z, float* scale_factor) {
    int o_dim_x, o_dim_y, o_dim_z;
    float o_translate_x, o_translate_y, o_translate_z, o_scale_factor;
    unsigned char* voxels = read_binvox_ii(filename, &o_dim_x, &o_dim_y, &o_dim_z, &o_translate_x, &o_translate_y, &o_translate_z, &o_scale_factor);
    *dim_x = o_dim_x;
    *dim_y = o_dim_y;
    *dim_z = o_dim_z;
    *translate_x = o_translate_x;
    *translate_y = o_translate_y;
    *translate_z = o_translate_z;
    *scale_factor = o_scale_factor;
    return voxels;
}