#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "vector_pointer.h"

vector_pointer* vector_pointer_create() {
    vector_pointer* v = (vector_pointer*) malloc(sizeof (vector_pointer));
    vector_pointer_init(v);
    return v;
}

void vector_pointer_init(vector_pointer* vector) {
    vector->size = 0;
    vector->capacity = VECTOR_INITIAL_CAPACITY;
    vector->data = (void**) malloc(sizeof (void*) * vector->capacity);
}

void vector_pointer_append(vector_pointer* vector, void* value) {
    vector_pointer_double_capacity_if_full(vector);
    vector->data[vector->size] = value;
    vector->size = vector->size + 1;
}

void* vector_pointer_get(vector_pointer* vector, int index) {
    if (index >= vector->size || index < 0) {
        printf("index: %d out of range\nmax vector size: %d\n", index, vector->size);
        printf("Unable to return value - exiting...\n");
        exit(-1);
    } else {
        return vector->data[index];
    }
}

void vector_pointer_set(vector_pointer* vector, int index, void* value) {
    if (index >= vector->size || index < 0) {
        printf("index: %d out of range\nmax vector size: %d\n", index, vector->size);
    }
    vector->data[index] = value;
}

void vector_pointer_double_capacity_if_full(vector_pointer* vector) {
    if (vector->size >= vector->capacity) {
        vector->capacity = vector->capacity * 2;
        vector->data = realloc(vector->data, sizeof (void*) * vector->capacity);
    }
}

void vector_pointer_free(vector_pointer* vector) {
    free(vector->data);
}
