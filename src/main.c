#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#ifdef __WIN32
#include <windows.h>
#endif
#include <time.h>
#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <GLUT/glut.h>
#include <OpenCL/cl.h>
#else
#include <GL/glut.h>
#include <CL/cl.h>
#endif

#include "p_graphics.h"
#include "vector_3d.h"
#include "octree.h"
#include "vector_pointer.h"
#include "world.h"
#include "p_util.h"
#include "camera.h"

int frames = 0;
int timef = 0;
int timebase = 0;
camera cam;
world scene;
bool mouse_down = false;

//how do you read a file in C
extern const char* read_file_c(const char* filename);

void init_cl() {
    error = clGetPlatformIDs(0, NULL, &num_platforms);
    if (error != CL_SUCCESS) {
        printf("unable to get platorms\n");
        exit(-1);
    }
    platforms = (cl_platform_id*) malloc(sizeof (cl_platform_id) * num_platforms);
    error = clGetPlatformIDs(num_platforms, platforms, NULL);
    error = clGetDeviceIDs(platforms[0], CL_DEVICE_TYPE_DEFAULT, 0, NULL, &num_devices);
    if (error != CL_SUCCESS) {
        printf("unable to get devices\n");
        exit(-1);
    }
    devices = (cl_device_id*) malloc(sizeof (cl_device_id) * num_devices);
    error = clGetDeviceIDs(platforms[0], CL_DEVICE_TYPE_DEFAULT, num_devices, devices, NULL);
    context = clCreateContext(NULL, num_devices, devices, NULL, NULL, &error);
    if (error != CL_SUCCESS) {
        printf("unable to create context\n");
        exit(-1);
    }
    command_queue = clCreateCommandQueue(context, *devices, 0, &error);
    if (error != CL_SUCCESS) {
        printf("unable to create command queue\n");
        exit(-1);
    }
    const char* kernel_src = read_file_c("kernel.cl");
    program = clCreateProgramWithSource(context, 1, &kernel_src, NULL, &error);
    if (error != CL_SUCCESS) {
        printf("unable to create program\n");
        exit(-1);
    }
    clBuildProgram(program, num_devices, devices, NULL, NULL, NULL);
    if (error != CL_SUCCESS) {
        printf("unable to build program\n");
        exit(-1);
    }
    char buffer[2048];
    size_t length;
    clGetProgramBuildInfo(program, devices[0], CL_PROGRAM_BUILD_LOG, sizeof (buffer), buffer, &length);
    printf((const char*) buffer);
    printf("\n");
    kernel = clCreateKernel(program, "cl_render_point", &error);
    if (error != CL_SUCCESS) {
        printf("unable to find kernel\n");
        printf("error: %d\n", error);
        exit(-1);
    }
    printf("opencl kernel built and initialized\n");
    octree_max_size = 0;
    int s_d;
    for (s_d = 0; s_d < octree_traverse_depth; s_d = s_d + 1) {
        int add_amt = (float) 14 * (float) pow((float) 8, (float) (s_d));
        octree_max_size = octree_max_size + add_amt;
    }
    angle_array_d = clCreateBuffer(context, CL_MEM_READ_ONLY, (frame_width * frame_height * 2) * sizeof (float), NULL, NULL);
    render_width_d = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof (int), NULL, NULL);
    render_height_d = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof (int), NULL, NULL);
    test_d = clCreateBuffer(context, CL_MEM_READ_WRITE, octree_max_size * sizeof (float), NULL, NULL);
    ray_pos_d = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof (vector_3d), NULL, NULL);
    ray_angle_d = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof (vector_3d), NULL, NULL);
    traverse_depth_d = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof (int), NULL, NULL);
    render_buffer_d = clCreateBuffer(context, CL_MEM_READ_WRITE, (frame_width * frame_height) * 3 * sizeof (float), NULL, NULL);
    printf("opencl buffers created\n");
    clEnqueueWriteBuffer(command_queue, angle_array_d, CL_TRUE, 0, (frame_width * frame_height * 2) * sizeof (float), angle_array, 0, NULL, NULL);
    clFinish(command_queue);
    clEnqueueWriteBuffer(command_queue, render_width_d, CL_TRUE, 0, sizeof (int), &frame_width, 0, NULL, NULL);
    clFinish(command_queue);
    clEnqueueWriteBuffer(command_queue, render_height_d, CL_TRUE, 0, sizeof (int), &frame_height, 0, NULL, NULL);
    clFinish(command_queue);
    clEnqueueWriteBuffer(command_queue, traverse_depth_d, CL_TRUE, 0, sizeof (int), &octree_traverse_depth, 0, NULL, NULL);
    clFinish(command_queue);
    printf("opencl static buffers created and information written\n");
    clSetKernelArg(kernel, 0, sizeof (cl_mem), &angle_array_d);
    clSetKernelArg(kernel, 1, sizeof (cl_mem), &render_width_d);
    clSetKernelArg(kernel, 2, sizeof (cl_mem), &render_height_d);
    clSetKernelArg(kernel, 6, sizeof (cl_mem), &traverse_depth_d);
    clSetKernelArg(kernel, 7, sizeof (cl_mem), &render_buffer_d);
    printf("opencl kernel arguments set\n");
}

void init_gl() {
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_COLOR_MATERIAL);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glClearColor(0, 0, 0, 0);
    glViewport(0, 0, frame_width, frame_height);
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluOrtho2D(0, frame_width, 0, frame_height);
    glScalef(1, -1, 1);
    glTranslatef(0, -frame_height, 0);
    glMatrixMode(GL_MODELVIEW);
    glClearColor(0.0, 0.0, 0.0, 1.0);
}

void display() {
    cl_renderAABB(frame, frame_width, frame_height, &cam, &scene);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glBegin(GL_2D);
    glDrawPixels(frame_width, frame_height, GL_RGB, GL_FLOAT, frame);
    frames = frames + 1;
    timef = glutGet(GLUT_ELAPSED_TIME);
    if (timef - timebase > 1000) {
        printf("FPS:%4.2f\n", frames * 1000.0 / (timef - timebase));
        timebase = timef;
        frames = 0;
    }
    glEnd();
    glutSwapBuffers();

}
int m_x = 0;
int m_y = 0;
int m_x_diff = 0;
int m_y_diff = 0;
vector_3d* rot_mouse;

void mouse(int button, int state, int x, int y) {
    if (state == 0) {
        m_x = x;
        m_y = y;
        mouse_down = true;
    } else {
        mouse_down = false;
    }
}

void mouse_motion(int x, int y) {
    if (mouse_down == true) {
        m_y_diff = (x - m_x) / (float) 8;
        m_x_diff = (y - m_y) / (float) 8;
        rot_mouse = rotate_degree_x(&cam.rotation, m_x_diff);
        rot_mouse = rotate_degree_y(rot_mouse, m_y_diff);
        cam.rotation = *rot_mouse;
        m_x = x;
        m_y = y;
        glutPostRedisplay();
    }
}

void print_camera_location() {
    printf("camera position %f %f %f\ncamera rotation %f %f %f\n", cam.position.x, cam.position.y, cam.position.z, cam.rotation.x, cam.rotation.y, cam.rotation.z);
}

float RandomFloat(float a, float b) {
    float random = ((float) rand()) / (float) RAND_MAX;
    float diff = b - a;
    float r = random * diff;
    return a + r;
}

void insert_random_point() {
    printf("inserting new point\n");
    vector_3d* new_random = vector_3d_create_default();
    new_random->x = RandomFloat((float) - 1, (float) 1);
    new_random->y = RandomFloat((float) - 1, (float) 1);
    new_random->z = RandomFloat((float) - 1, (float) 1);
    octree* first_octree = vector_pointer_get(&scene, 0);
    octree_insert(first_octree, new_random);
}
float increment = 1.5;

void keyboard(unsigned char key, int x, int y) {
    int key_code = (int) key;
    switch (key_code) {
        case 27:
            printf("Exiting...\n");
            exit(0);
        case 49:
            //set_solid_color(0,0,0);
            insert_random_point();
            break;
        case 50:
            //set_solid_color(1,1,1);
            break;
        case 119:
            cam.position.x = cam.position.x + (float) increment;
            print_camera_location();
            break;
        case 115:
            cam.position.x = cam.position.x - (float) increment;
            print_camera_location();
            break;
        case 97:
            cam.position.z = cam.position.z - (float) increment;
            print_camera_location();
            break;
        case 100:
            cam.position.z = cam.position.z + (float) increment;
            print_camera_location();
            break;
        case 105:
            cam.rotation.x = -cam.rotation.x;
            print_camera_location();
            break;
        case 101:
            cam.position.y = cam.position.y + (float) increment;
            print_camera_location();
            break;
        case 113:
            cam.position.y = cam.position.y - (float) increment;
            print_camera_location();
            break;
        default:
            break;
    }
}

void set_variables() {
    frame_width = 1280;
    frame_height = 720;
    field_of_view = 60;
    octree_traverse_depth = 5;
    printf("setting frame_width %d and frame_height %d\n", frame_width, frame_height);
    frame = (float*) malloc(sizeof (float)*frame_height * frame_width * 3);
    printf("gl frame buffer created\n");
    calculate_angles(frame_width, frame_height, field_of_view);
    printf("calculated angle offsets\n");
}

void init_scene() {
    cam = *camera_create_default();
    cam.position.x = (float) - 10;
    scene = *world_create_default();
    vector_3d* o_origin = vector_3d_create_default();
    vector_3d* o_half_dimension = vector_3d_create(1, 1, 1);
    octree* o = octree_create(o_origin, o_half_dimension);
    vector_3d* test_point = vector_3d_create((float) 0.2, (float) 0, (float) - 0.2);
    octree_insert(o, test_point);
    vector_3d* other_point = vector_3d_create((float) - 0.1, (float) 0.1, (float) 0.2);
    octree_insert(o, other_point);
    octree* o2 = octree_create_default();
    o2->origin.y = (float) 5;
    octree* o3 = octree_create_default();
    o3->origin.y = (float) - 5;
    o3->origin.x = (float) 10;
    octree_insert(o2, &o2->origin);
    vector_pointer_append(&scene.octrees, o);
    //vector_pointer_append(&scene.octrees, o2);
    //vector_pointer_append(&scene.octrees, o3);
    vector_pointer* test_get = vector_pointer_create();
    vector_3d* o_bmin = vector_3d_create(o->origin.x - o->half_dimension.x, o->origin.y - o->half_dimension.y, o->origin.z - o->half_dimension.z);
    vector_3d* o_bmax = vector_3d_create(o->origin.x + o->half_dimension.x, o->origin.y + o->half_dimension.y, o->origin.z + o->half_dimension.z);
    octree_get_box_points(o, *o_bmin, *o_bmax, test_get);
    printf("test_get size is %d\n", test_get->size);
    int i = 0;
    for (i = 0; i < test_get->size; i = i + 1) {
        vector_3d* get_test = vector_pointer_get(test_get, i);
        printf("get_test %p\n", get_test);
        printf("gt %f %f %f\n", get_test->x, get_test->y, get_test->z);
    }

}

int main(int argc, char** argv) {
    octree* test = octree_create_default();
    vector_3d* a = vector_3d_create(0.25, 0.25, 0.25);
    vector_3d* b = vector_3d_create(-0.25, 0.25, 0.25);
    vector_3d* c = vector_3d_create(0.25, -0.25, 0.25);
    vector_3d* d = vector_3d_create(-0.25, -0.25, 0.25);
    vector_3d* e = vector_3d_create(0.25, 0.25, -0.25);
    vector_3d* f = vector_3d_create(-0.25, 0.25, -0.25);
    vector_3d* g = vector_3d_create(0.25, -0.25, -0.25);
    vector_3d* h = vector_3d_create(-0.25, -0.25, -0.25);
    printf("%d %d %d %d %d %d %d %d\n", octree_get_octant(test, *a), octree_get_octant(test, *b), octree_get_octant(test, *c), octree_get_octant(test, *d), octree_get_octant(test, *e), octree_get_octant(test, *f), octree_get_octant(test, *g), octree_get_octant(test, *h));
    //output is 7 3 5 1 6 2 4 0
    printf("hello world\n");
    printf("setting variables\n");
    set_variables();
    printf("creating scene\n");
    init_scene();
    printf("creating gl window...\n");
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
    glutInitWindowSize(frame_width, frame_height);
    glutCreateWindow("gl window");
    glutDisplayFunc(display);
    glutIdleFunc(display);
    glutKeyboardFunc(keyboard);
    glutMouseFunc(mouse);
    glutMotionFunc(mouse_motion);
    init_gl();
    init_cl();
    glutMainLoop();
    return 0;
}
