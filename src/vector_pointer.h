#ifndef vector_pointer_h
#define vector_pointer_h
#define VECTOR_INITIAL_CAPACITY 10

typedef struct {
    int size;
    int capacity;
    void** data;
} vector_pointer;
vector_pointer* vector_pointer_create();
void vector_pointer_init(vector_pointer* vector);
void vector_pointer_append(vector_pointer* vector, void* value);
void* vector_pointer_get(vector_pointer* vector, int index);
void vector_pointer_set(vector_pointer* vector, int index, void* value);
void vector_pointer_double_capacity_if_full(vector_pointer* vector);
void vector_pointer_free(vector_pointer* vector);

#endif
