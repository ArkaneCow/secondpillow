#include "vector_3d.h"
void vector_3d_init(vector_3d* v, float x, float y, float z)
{
	v->x = x;
	v->y = y;
	v->z = z;
}
void vector_3d_init_default(vector_3d* v)
{
	vector_3d_init(v, 0,0,0);
}
vector_3d* vector_3d_create(float x, float y, float z)
{
	vector_3d* v = (vector_3d*)malloc(sizeof(vector_3d));
	vector_3d_init(v, x, y, z);
	return v;
}
vector_3d* vector_3d_create_default()
{
	vector_3d* v = (vector_3d*)malloc(sizeof(vector_3d));
	vector_3d_init_default(v);
	return v;
}
float vector_3d_magnitude(vector_3d* a)
{
	return sqrt((a->x*a->x) + (a->y*a->y) + (a->z*a->z));
}
vector_3d vector_3d_normalize(vector_3d* a)
{
	float magnitude = sqrt((a->x*a->x) + (a->y*a->y) + (a->z*a->z));
	vector_3d n;
	n.x = a->x/magnitude;
	n.y = a->y/magnitude;
	n.z = a->z/magnitude;
	return n;
}
vector_3d vector_3d_negative(vector_3d* a)
{
	vector_3d n;
	n.x = -a->x;
	n.y = -a->y;
	n.z = -a->z;
	return n;
}
float vector_3d_dot_product(vector_3d* a, vector_3d* v)
{
	return a->x*v->x + a->y*v->y + a->z*v->z;
}
vector_3d vector_3d_cross_product(vector_3d* a, vector_3d* v)
{
	vector_3d c;
	c.x = a->y*v->z - a->z*v->y;
	c.y = a->z*v->x - a->x*v->z;
	c.z = a->x*v->y - a->y*v->x;
	return c;
}
vector_3d vector_3d_vector_add(vector_3d* a, vector_3d* v)
{
	vector_3d s;
	s.x = a->x + v->x;
	s.y = a->y + v->y;
	s.z = a->z + v->z;
	return s;
}
vector_3d vector_3d_vector_subtract(vector_3d* a, vector_3d* v)
{
	vector_3d d;
	d.x = a->x - v->x;
	d.y = a->y - v->y;
	d.z = a->z - v->z;
	return d;
}
vector_3d vector_3d_vector_multiply(vector_3d* a, float scalar)
{
	vector_3d p;
	p.x = a->x*scalar;
	p.y = a->y*scalar;
	p.z = a->z*scalar;
	return p;
}
vector_3d vector_3d_vector_divide(vector_3d* a, float scalar)
{
	vector_3d q;
	q.x = a->x/scalar;
	q.y = a->y/scalar;
	q.z = a->z/scalar;
	return q;
}
float vector_3d_angle_between(vector_3d* a, vector_3d* b)
{
	float dot_product = vector_3d_dot_product(a,b);
	float mag_product = (sqrt(((a->x)*(a->x))+((a->y)*(a->y))+((a->z)*(a->z))))+(sqrt(((b->x)*(b->x))+((b->y)*(b->y))+((b->z)*(b->z))));
	return acos((dot_product/mag_product));
}
vector_3d* rotate_degree_x(vector_3d* o, float degree)
{
	float x = o->x;
	float y = o->y;
	float z = o->z;
	float rad = (float)degree*(float)((float)3.14/(float)180);
	float n_x = x;
	float n_y = (cos(rad)*y)-(sin(rad)*z);
	float n_z = (sin(rad)*y)+(cos(rad)*z);
	vector_3d* new_vector = vector_3d_create(n_x, n_y, n_z);
	return new_vector;
}
vector_3d* rotate_degree_y(vector_3d* o, float degree)
{
	float x = o->x;
	float y = o->y;
	float z = o->z;
	float rad = (float)degree*(float)((float)3.14/(float)180);
	float n_x = (cos(rad)*x) + (sin(rad)*z);
	float n_y = y;
	float n_z = (cos(rad)*z) - (sin(rad)*x);
	vector_3d* new_vector = vector_3d_create(n_x, n_y, n_z);
	return new_vector;
}
vector_3d* rotate_degree_z(vector_3d* o, float degree)
{
	float x = o->x;
	float y = o->y;
	float z = o->z;
	float rad = (float)degree*(float)((float)3.14/(float)180);
	float n_x = (cos(rad)*x) - (sin(rad)*y);
	float n_y = (sin(rad)*x) + (cos(rad)*y);
	float n_z = z;
	vector_3d* new_vector = vector_3d_create(n_x, n_y, n_z);
	return new_vector;
}