#include "octree.h"
#include "vector_pointer.h"

void octree_init(octree* o, vector_3d* set_origin, vector_3d* set_half_dimension) {
    o->origin = *set_origin;
    o->half_dimension = *set_half_dimension;
    o->data = NULL;
    int i;
    for (i = 0; i < 8; i = i + 1) {
        o->children[i] = NULL;
    }
    printf("pointer location %p\n", o);
}

void octree_init_default(octree* o) {
    vector_3d* origin = vector_3d_create_default();
    vector_3d* hf = vector_3d_create(0.5, 0.5, 0.5);
    octree_init(o, origin, hf);
}

octree* octree_create(vector_3d* origin, vector_3d* half_dimension) {
    octree* o = (octree*) malloc(sizeof (octree));
    octree_init(o, origin, half_dimension);
    return o;
}

octree* octree_create_default() {
    octree* o = (octree*) malloc(sizeof (octree));
    octree_init_default(o);
    return o;
}

vector_3d octree_get_full_dimension(octree* o) {
    float x_diff = o->half_dimension.x - o->origin.x;
    float y_diff = o->half_dimension.y - o->origin.y;
    float z_diff = o->half_dimension.z - o->origin.z;
    float new_x = o->origin.x + (2 * x_diff);
    float new_y = o->origin.y + (2 * y_diff);
    float new_z = o->origin.z + (2 * z_diff);
    vector_3d f;
    f.x = new_x;
    f.y = new_y;
    f.z = new_z;
    return f;
}

int octree_get_octant(octree* o, vector_3d point) {
    int oct = 0;
    if (point.x >= o->origin.x) oct |= 4;
    if (point.y >= o->origin.y) oct |= 2;
    if (point.z >= o->origin.z) oct |= 1;
    return oct;
}

bool octree_is_leaf(octree* o) {
    return o->children[0] == NULL;
}

void octree_insert(octree* o, vector_3d* p) {
    if (octree_is_leaf(o)) {
        if (o->data == NULL) {
            o->data = p;
            printf("point added %f %f %f\n", o->data->x, o->data->y, o->data->z);
            return;
        } else {
            vector_3d* old_point = o->data;
            o->data = NULL;
            int i;
            for (i = 0; i < 8; i = i + 1) {
                o->children[i] = (octree*) malloc(sizeof (octree));
                o->children[i]->origin.x = o->origin.x + o->half_dimension.x * (i & 4 ? .5f : -.5f);
                o->children[i]->origin.y = o->origin.y + o->half_dimension.y * (i & 2 ? .5f : -.5f);
                o->children[i]->origin.z = o->origin.z + o->half_dimension.z * (i & 1 ? .5f : -.5f);
                o->children[i]->half_dimension = vector_3d_vector_multiply(&o->half_dimension, 0.5f);
                o->children[i]->data = NULL;
                int a;
                for (a = 0; a < 8; a = a + 1) {
                    o->children[i]->children[a] = NULL;
                }
            }
            printf("%d %d\n", octree_get_octant(o, *old_point), octree_get_octant(o, *p));
            octree_insert(o->children[octree_get_octant(o, *old_point)], old_point);
            octree_insert(o->children[octree_get_octant(o, *p)], p);
        }
    } else {
        int octant = octree_get_octant(o, *p);
        octree_insert(o->children[octant], p);
    }
}

void octree_get_box_points(octree* o, vector_3d bmin, vector_3d bmax, vector_pointer* results) {
    if (octree_is_leaf(o)) {
        if (o->data != NULL) {
            vector_3d p = *o->data;
            if (p.x > bmax.x || p.y > bmax.y || p.z > bmax.z) {
                return;
            }
            if (p.x < bmin.x || p.y < bmin.y || p.z < bmin.z) {
                return;
            }
            //results.push_back(o->data);
            printf("data %f %f %f\n", o->data->x, o->data->y, o->data->z);
            vector_pointer_append(results, &o->data);
        }
    } else {
        int i = 0;
        for (i = 0; i < 8; i = i + 1) {
            vector_3d cmax = vector_3d_vector_add(&o->children[i]->origin, &o->children[i]->half_dimension);
            vector_3d cmin = vector_3d_vector_subtract(&o->children[i]->origin, &o->children[i]->half_dimension);


            if (cmax.x < bmin.x || cmax.y < bmin.y || cmax.z < bmin.z) {
                continue;
            }
            if (cmin.x > bmax.x || cmin.y > bmax.y || cmin.z > bmax.z) {
                continue;
            }
            octree_get_box_points(o->children[i], bmin, bmax, results);
        }
    }
}

bool octree_check(octree* o, vector_3d p) {
    float allowance = 0.1;
    vector_3d bound_min;
    vector_3d bound_max;
    bound_min.x = o->origin.x - o->half_dimension.x;
    bound_min.y = o->origin.y - o->half_dimension.y;
    bound_min.z = o->origin.z - o->half_dimension.z;
    bound_max.x = o->origin.x + o->half_dimension.x;
    bound_max.y = o->origin.y + o->half_dimension.y;
    bound_max.z = o->origin.z + o->half_dimension.z;
    float pos_x = p.x;
    float pos_y = p.y;
    float pos_z = p.z;
    bool hit = false;
    if
        (
            (pos_x >= bound_min.x &&
            pos_x <= bound_max.x &&
            pos_y >= bound_min.y &&
            pos_y <= bound_max.y &&
            pos_z >= bound_min.z &&
            pos_z <= bound_max.z) ||
            (pos_x + allowance >= bound_min.x &&
            pos_x - allowance <= bound_max.x &&
            pos_y + allowance >= bound_min.y &&
            pos_y - allowance <= bound_max.y &&
            pos_z + allowance >= bound_min.z &&
            pos_z - allowance <= bound_max.z)
            ) {
        hit = true;
    }
    return hit;
}
//http://gamedev.stackexchange.com/questions/18436/most-efficient-aabb-vs-ray-collision-algorithms
//zacharmarz

bool octree_intersect(octree* o, vector_3d* origin, vector_3d* direction, float* t_out, float* t_min_out, float* t_max_out) {
    vector_3d dirfrac;
    dirfrac.x = 1.0f / direction->x;
    dirfrac.y = 1.0f / direction->y;
    dirfrac.z = 1.0f / direction->z;
    vector_3d lb, rt;
    lb.x = vector_3d_vector_subtract(&o->origin, &o->half_dimension).x;
    lb.y = vector_3d_vector_subtract(&o->origin, &o->half_dimension).y;
    lb.z = vector_3d_vector_subtract(&o->origin, &o->half_dimension).z;
    rt.x = vector_3d_vector_add(&o->origin, &o->half_dimension).x;
    rt.y = vector_3d_vector_add(&o->origin, &o->half_dimension).y;
    rt.z = vector_3d_vector_add(&o->origin, &o->half_dimension).z;
    float t1 = (lb.x - origin->x) * dirfrac.x;
    float t2 = (rt.x - origin->x) * dirfrac.x;
    float t3 = (lb.y - origin->y) * dirfrac.y;
    float t4 = (rt.y - origin->y) * dirfrac.y;
    float t5 = (lb.z - origin->z) * dirfrac.z;
    float t6 = (rt.z - origin->z) * dirfrac.z;
    float tmin = fmax(fmax(fmin(t1, t2), fmin(t3, t4)), fmin(t5, t6));
    float tmax = fmin(fmin(fmax(t1, t2), fmax(t3, t4)), fmax(t5, t6));
    float t; //length until intersect
    if (tmax < 0) {
        t = tmax;
        return false;
    }
    if (tmin > tmax) {
        t = tmax;
        return false;
    }
    t = tmin;
    *t_out = t;
    *t_min_out = tmin;
    *t_max_out = tmax;
    //printf("t is %f\n", t);
    return true;
}

octree* octree_binvox(char* filename, float origin_x, float origin_y, float origin_z) {
    int dim_x, dim_y, dim_z;
    float translate_x, translate_y, translate_z, scale_factor;
    unsigned char* voxels = read_binvox_ii_c(filename, &dim_x, &dim_y, &dim_z, &translate_x, &translate_y, &translate_z, &scale_factor);
    unsigned int vox_size = dim_x * dim_y * dim_z;
    vector_3d* origin = vector_3d_create(origin_x, origin_y, origin_z);
    vector_3d* half_dimension = vector_3d_create((dim_x / 2), (dim_y / 2), (dim_z / 2));
    int points_index = 0;
    vector_3d* points = (vector_3d*) malloc(sizeof (vector_3d) * vox_size);
    octree* o = octree_create(origin, half_dimension);
    int a;
    for (a = 0; a < vox_size; a = a + 1) {
        if (((char) (voxels[a] + '0')) == '1') {
            vector_3d* vox = get_binvox_coord(a, dim_x, dim_y, dim_z);
            points[points_index] = *vox;
            points_index = points_index + 1;
        }
    }
    int b;
    for (b = 0; b < sizeof (points); b = b + 1) {
        printf("octree insert: %f %f %f\n", points[b].x, points[b].y, points[b].z);
        octree_insert(o, &points[b]);
    }
    printf("octree created from binvox\n");
    printf("o %f %f %f\nhf %f %f %f\n", o->origin.x, o->origin.y, o->origin.z, o->half_dimension.x, o->half_dimension.y, o->half_dimension.z);
    return o;
}

float* octree_to_linear_static_array(octree* o, int depth, int* size) {
    //i should be using huffman trees or something
    int array_length = 0;
    int i;
    for (i = 0; i < depth; i = i + 1) {
        int l_length = (int) ((float) 14 * (float) pow((float) 8, (float) (depth - 1)));
        array_length = array_length + l_length;
    }
    float* o_array = (float*) malloc(array_length * sizeof (float));
    int d;
    for (d = 0; d < depth; d = d + 1) {
        int level_start_index = 0;
        int s_d;
        for (s_d = 0; s_d < d; s_d = s_d + 1) {
            int add_amt = (float) 14 * (float) pow((float) 8, (float) (s_d));
            level_start_index = level_start_index + add_amt;
        }
        int num_octrees = (int) (pow((float) 8, (float) (d)));
        int o_n;
        for (o_n = 0; o_n < num_octrees; o_n = o_n + 1) {
            int octree_start_index = 8 * o_n;
            int start_index = level_start_index + octree_start_index;
            int o_s_d;
            octree* o_data = o;
            int octant_path[d];
            for (o_s_d = 0; o_s_d < d; o_s_d = o_s_d + 1) {
                int level_size = (int) ((float) 14 * (float) pow((float) 8, (float) (o_s_d)));
                int divisor = (int) pow((float) 8, (float) o_s_d);
                int quotient = level_size / divisor;
                int m;
                for (m = 0; m < 8; m = m + 1) {
                    if ((start_index >= (m * quotient)) && (start_index < ((m + 1) * quotient))) {
                        octant_path[o_s_d] = m;
                    }
                }
            }
            bool path_break = false;
            if (octree_is_leaf(o_data)) {
                path_break = true;
            }
            int p_s_d;
            for (p_s_d = 0; p_s_d < d; p_s_d = p_s_d + 1) {
                if (path_break == false) {
                    if (octree_is_leaf(o_data)) {
                        path_break = true;
                    } else {
                        o_data = o_data->children[octant_path[p_s_d]];
                    }
                }
            }
            if (path_break) {
                o_array[start_index] = (float) 0;
            } else {
                o_array[start_index] = (float) 1;
                o_array[start_index + 1] = o_data->origin.x;
                o_array[start_index + 2] = o_data->origin.y;
                o_array[start_index + 3] = o_data->origin.z;
                o_array[start_index + 4] = o_data->half_dimension.x;
                o_array[start_index + 5] = o_data->half_dimension.y;
                o_array[start_index + 6] = o_data->half_dimension.z;
                if (o_data->data == NULL)
                {
                    o_array[start_index + 7] = (float) 0;
                }
                else
                {
                    o_array[start_index + 7] = (float) 1;
                    o_array[start_index + 8] = o_data->data->x;
                    o_array[start_index + 9] = o_data->data->y;
                    o_array[start_index + 10] = o_data->data->z;
                    o_array[start_index + 11] = (float) 1;
                    o_array[start_index + 12] = (float) 0;
                    o_array[start_index + 13] = (float) 0;
                }
            }
        }
    }
    *size = array_length * sizeof (float);
    return o_array;
}