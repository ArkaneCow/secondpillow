#include "camera.h"

void camera_init(camera* c, vector_3d position, vector_3d rotation) {
    c->position = position;
    c->rotation = rotation;
}

void camera_init_default(camera* c) {
    vector_3d* origin = vector_3d_create_default();
    vector_3d* x_one = vector_3d_create(1, 0, 0);
    c->position = *origin;
    c->rotation = *x_one;
}

camera* camera_create(vector_3d position, vector_3d rotation) {
    camera* c = (camera*) malloc(sizeof (camera));
    camera_init(c, position, rotation);
    return c;
}

camera* camera_create_default() {
    camera* c = (camera*) malloc(sizeof (camera));
    camera_init_default(c);
    return c;
}
